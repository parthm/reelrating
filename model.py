import pickle
from util import clean_text  # Import the clean_text function from util.py
import torch
from transformers import DistilBertForSequenceClassification, DistilBertTokenizer

def load_model(model_path):
    """ Load a trained model from a file. """
    with open(model_path, 'rb') as file:
        return pickle.load(file)

# Load your model and vectorizer
regression_model = load_model('saved_SVR_model.pkl')
regression_vectorizer = load_model('saved_SVR_vectorizer.pkl')

def predict_regression_rating(review_text):
    """ Predict the rating for a given review. """
    cleaned_review = clean_text(review_text)
    review_tfidf = regression_vectorizer.transform([cleaned_review])
    return (regression_model.predict(review_tfidf)[0] * 10).round(decimals=1)

def load_transformer_model(model_path):
    """ Load a trained transformer model. """
    model = DistilBertForSequenceClassification.from_pretrained(model_path)
    return model

def load_transformer_tokenizer(tokenizer_path):
    """ Load a trained transformer tokenizer. """
    tokenizer = DistilBertTokenizer.from_pretrained(tokenizer_path)
    return tokenizer

# Assuming you have the correct directory for your saved model and tokenizer:
BERT_model = load_transformer_model('DistilBERT_model')
BERT_tokenizer = load_transformer_tokenizer('DistilBERT_tokenizer')

def predict_BERT_rating(review_text):
    """ Predict the rating for a given review using DistilBERT. """
    cleaned_review = clean_text(review_text)
    inputs = BERT_tokenizer.encode_plus(
        cleaned_review,
        None,
        add_special_tokens=True,
        max_length=256,
        padding='max_length',
        return_token_type_ids=False,
        return_attention_mask=True,
        truncation=True,
        return_tensors='pt'
    )

    # Move tensors to the same device as the model
    inputs = {key: value.to(BERT_model.device) for key, value in inputs.items()}
    
    # Get model predictions
    with torch.no_grad():
        outputs = BERT_model(**inputs)
    
    # Convert model logits to probabilities and then to the desired scale
    probs = torch.nn.functional.softmax(outputs.logits, dim=-1)
    rating = torch.argmax(probs, dim=-1).item()  # Assuming binary classification (0 or 1)
    
    return rating

def classify_review_with_BERT(review_text):
    """ Classify the review as positive, neutral, or negative using DistilBERT. """
    cleaned_review = clean_text(review_text)
    inputs = BERT_tokenizer.encode_plus(
        cleaned_review,
        None,
        add_special_tokens=True,
        max_length=256,
        padding='max_length',
        truncation=True,
        return_token_type_ids=False,
        return_attention_mask=True,
        return_tensors='pt'
    )

    # Move tensors to the same device as the model
    inputs = {key: value.to(BERT_model.device) for key, value in inputs.items()}
    
    # Get model predictions
    with torch.no_grad():
        outputs = BERT_model(**inputs)

    probs = torch.nn.functional.softmax(outputs.logits, dim=-1)
    sentiment_class = torch.argmax(probs, dim=-1).item()  # 0 or 1

    # Classify as positive, neutral, or negative
    if sentiment_class == 1:
        return "positive"
    else:
        return "negative"


def get_combined_rating(review_text):
    """ Get a combined rating using both DistilBERT and SVR models. """
    sentiment = classify_review_with_BERT(review_text)
    
    # Get SVR model's prediction
    svr_rating = predict_regression_rating(review_text)

    # Adjust the SVR rating based on the sentiment classification
    if sentiment == "positive":
        # If positive, use the upper range of the SVR model's rating
        adjusted_rating = svr_rating + 5  
    else:
        # If negative, use the lower range of the SVR model's rating
        adjusted_rating = svr_rating / 2 + 2.5

    # Ensure the rating is within bounds
    adjusted_rating = max(1, min(10, adjusted_rating))

    return round(adjusted_rating, 1)

