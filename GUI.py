import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkinter import font as tkfont  # for font styling
from model import predict_regression_rating, classify_review_with_BERT, get_combined_rating

def get_regression_movie_rating():
    """Get the movie review from the user and display the predicted rating from the SVR model."""
    review_text = review_text_entry.get("1.0", "end-1c")  # Get the review text from the text widget
    
    if review_text:
        prediction = predict_regression_rating(review_text)  # Get the predicted rating from SVR
        messagebox.showinfo("SVR Predicted Rating", f"The predicted rating is: {prediction} out of 10")
    else:
        messagebox.showerror("Error", "Please enter a movie review.")

def get_BERT_movie_classification():
    """Get the movie review from the user and display the sentiment classification from the DistilBERT model."""
    review_text = review_text_entry.get("1.0", "end-1c")  # Get the review text from the text widget
    
    if review_text:
        sentiment = classify_review_with_BERT(review_text)  # Classify the review using DistilBERT
        messagebox.showinfo("DistilBERT Sentiment Analysis", f"The review sentiment is classified as: {sentiment}")
    else:
        messagebox.showerror("Error", "Please enter a movie review.")

def get_combined_movie_rating():
    """Get the movie review from the user and display the combined rating from both models."""
    review_text = review_text_entry.get("1.0", "end-1c")  # Get the review text from the text widget
    
    if review_text:
        combined_rating = get_combined_rating(review_text)  # Get the combined rating
        messagebox.showinfo("Combined Model Predicted Rating", f"The predicted rating is: {combined_rating} out of 10")
    else:
        messagebox.showerror("Error", "Please enter a movie review.")

# Create a window
root = tk.Tk()
root.title("ReelRatings")
root.geometry("700x500")  # Set the window size
root.configure(bg='light yellow')  # Set the background color of the window

# Use a nicer theme if available
style = ttk.Style()
style.theme_use('clam')  

# Set a custom font
custom_font = tkfont.Font(family="Helvetica", size=12)
header_font = tkfont.Font(family="Helvetica", size=16, weight="bold")

# Header
header = ttk.Label(root, text="ReelRatings", font=header_font)
header.pack(pady=(10, 20))

# Widget container
widget_container = tk.Frame(root)  # No background color specified
widget_container.pack(pady=20)  # Add some vertical padding

# Create a text widget for entering the movie review
review_text_label = ttk.Label(widget_container, text="Enter your movie review:", font=custom_font)
review_text_label.pack()

review_text_entry = tk.Text(widget_container, height=10, width=50)
review_text_entry.pack()

# Create a button to get the predicted SVR rating
get_regression_rating_button = ttk.Button(widget_container, text="Get SVR Rating", command=get_regression_movie_rating)
get_regression_rating_button.pack(side=tk.LEFT, padx=10)

# Create a button to get the sentiment classification from DistilBERT
get_BERT_classification_button = ttk.Button(widget_container, text="Get DistilBERT Sentiment", command=get_BERT_movie_classification)
get_BERT_classification_button.pack(side=tk.LEFT, padx=10)

# Create a button to get the combined rating
get_combined_rating_button = ttk.Button(widget_container, text="Get Combined Rating", command=get_combined_movie_rating)
get_combined_rating_button.pack(side=tk.RIGHT, padx=10)

# Start the GUI
root.mainloop()
